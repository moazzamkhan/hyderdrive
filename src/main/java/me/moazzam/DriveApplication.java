package me.moazzam;

import me.moazzam.model.Account;
import me.moazzam.model.Item;
import me.moazzam.repository.AccountRepository;
import me.moazzam.repository.ItemRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class DriveApplication {

    public static void main(String[] args) {
        SpringApplication.run(DriveApplication.class, args);
    }

    @Bean
    CommandLineRunner init(AccountRepository accountRepository, ItemRepository itemRepository) {
        return (evt) ->
                Arrays.asList("jhoeller,dsyer,pwebb,ogierke,rwinch,mfisher,mpollack,jlong, moazzam".split(","))
                        .forEach(a -> {
                            Account account = accountRepository.save(new Account(a, "password"));
                            itemRepository.save(new Item("f1", account.getUsername()));
                            itemRepository.save(new Item("f2", account.getUsername()));
                        });
    }
}
