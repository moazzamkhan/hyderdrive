package me.moazzam;

import me.moazzam.model.Revision;

import javax.persistence.PrePersist;
import java.util.Date;

public class RevisionEntityListener {
    @PrePersist
    public void prePersist(Revision revision) {
        revision.setCreatedOn(new Date());
    }
}
