package me.moazzam.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Content {
    @Id
    @GeneratedValue
    private Long id;

    private byte[] content;

    public Content() {}

    public Content(byte[] content) {
        this.content = content;
    }

    public Content(String s) {
        this.content = s.getBytes();
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
