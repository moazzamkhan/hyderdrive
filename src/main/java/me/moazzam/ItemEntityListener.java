package me.moazzam;

import me.moazzam.model.Item;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

/**
 * Created by Khan on 5/21/2016.
 */
public class ItemEntityListener {
    @PreUpdate
    public void preUpdate(Item item) {
        item.setLastModifiedOn(new Date());
    }

    @PrePersist
    public void prePersist(Item item) {
        Date d = new Date();
        item.setLastModifiedOn(d);
        item.setCreatedOn(d);
        item.setlastModifiedBy(item.getCreatedBy());
    }

    @PostPersist
    public void postPersist(Item item) {
        item.setUri("/" + item.getId());
    }
}
