package me.moazzam.resource;

import me.moazzam.model.Item;
import me.moazzam.ItemRestController;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


public class ItemResource extends ResourceSupport {
    private final Item item;

    public ItemResource(Item item) {
        this.item = item;
        this.add(new Link(item.getUri(), "item-uri"));
        this.add(linkTo(ItemRestController.class).withRel("items"));
        this.add(ControllerLinkBuilder.linkTo(methodOn(ItemRestController.class).readItem(item.getId())).withSelfRel());
    }

    public Item getItem() {
        return item;
    }
}
