package me.moazzam;

import me.moazzam.model.Account;
import me.moazzam.model.Content;
import me.moazzam.model.Item;
import me.moazzam.model.Revision;
import me.moazzam.repository.AccountRepository;
import me.moazzam.repository.ContentRepository;
import me.moazzam.repository.ItemRepository;
import me.moazzam.repository.RevisionRepository;
import me.moazzam.resource.ItemResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/items")
public class ItemRestController {

    private final ItemRepository itemRepository;
    private final AccountRepository accountRepository;
    private final ContentRepository contentRepository;
    private final RevisionRepository revisionRepository;

    @Autowired
    public ItemRestController(ItemRepository itemRepository, AccountRepository accountRepository, ContentRepository contentRepository, RevisionRepository revisionRepository) {
        this.itemRepository = itemRepository;
        this.accountRepository = accountRepository;
        this.contentRepository = contentRepository;
        this.revisionRepository = revisionRepository;
    }

    /**
     * Insert a new file
     *
     * @param item
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addItem(@RequestBody Item item) {
        Account user = this.validateUser();
        item.setCreatedBy(user.getUsername());

        item = itemRepository.save(item);

        HttpHeaders httpHeaders = new HttpHeaders();
        ItemResource ir = new ItemResource(item);
        Link forOneItem = ir.getLink("self");
        httpHeaders.setLocation(URI.create(forOneItem.getHref()));

        return new ResponseEntity<>(ir, httpHeaders, HttpStatus.CREATED);
    }

    /**
     * Lists the user's files
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Resources<ItemResource> listItems() {
        Account user = this.validateUser();
        List<ItemResource> itemResourceList = itemRepository.findAll()
                .stream()
                .map(ItemResource::new)
                .collect(Collectors.toList());
        return new Resources<ItemResource>(itemResourceList);
    }

    /**
     * Gets a file's metadata by ID
     *
     * @param itemId
     * @return
     */
    @RequestMapping(value = "/{itemId}", method = RequestMethod.GET)
    public ItemResource readItem(@PathVariable Long itemId) {
        this.validateUser();
        return new ItemResource(this.itemRepository.findOne(itemId));
    }

    /**
     * only displayName, description, content can be updated through this endpoint
     *
     * @param itemId
     * @param patch
     * @return
     */
    @RequestMapping(value = "/{itemId}", method = {RequestMethod.PATCH, RequestMethod.PUT})
    public ItemResource patchItem(@PathVariable Long itemId, @RequestBody Map patch) {
        Account user = this.validateUser();
        Item item = this.itemRepository.findOne(itemId);

        //update changes table
        if (patch.containsKey("displayName")) {
            item.setDisplayName((String) patch.get("displayName"));
            item.setlastModifiedBy(user.getUsername());
        }

        //update changes table
        if (patch.containsKey("description")) {
            item.setDescription((String) patch.get("description"));
            item.setlastModifiedBy(user.getUsername());
        }

        if (patch.containsKey("content")) {
            System.out.println((String) patch.get("content"));

            //save new content
            Content content = new Content((String) patch.get("content"));
            contentRepository.save(content);

            // add a revision entry
            Revision revision = new Revision(item.getId(), content.getId(), user.getUsername(), "Some changes done by me");
            revisionRepository.save(revision);

            //update item
            item.setRevisionId(revision.getId());
            item.setlastModifiedBy(user.getUsername());
        }

        return new ItemResource(item);
    }


    @RequestMapping(value = "/{itemId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteItem(@PathVariable Long itemId) {
        this.validateUser();
        this.itemRepository.delete(itemId);
        return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
    }


    @RequestMapping(value = "/{itemId}/copy", method = RequestMethod.POST)
    public ResponseEntity copyItem(@PathVariable Long itemId) {
        Account user = this.validateUser();
        Item item = this.itemRepository.findOne(itemId);
        item.setCreatedBy(user.getUsername());
        item.setId(null);
        itemRepository.save(item);

        HttpHeaders httpHeaders = new HttpHeaders();
        ItemResource ir = new ItemResource(item);
        Link forOneItem = ir.getLink("self");
        httpHeaders.setLocation(URI.create(forOneItem.getHref()));

        return new ResponseEntity<>(ir, httpHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{itemId}/trash", method = RequestMethod.POST)
    public ItemResource trashItem(@PathVariable Long itemId) {
        this.validateUser();
        return new ItemResource(this.itemRepository.findOne(itemId));
    }

    @RequestMapping(value = "/{itemId}/untrash", method = RequestMethod.POST)
    public ItemResource untrashItem(@PathVariable Long itemId) {
        this.validateUser();
        return new ItemResource(this.itemRepository.findOne(itemId));
    }

    @RequestMapping(value = "/trash", method = RequestMethod.DELETE)
    public ItemResource emptyTrash() {
        this.validateUser();
        return new ItemResource(this.itemRepository.findOne(null));
    }

    private Account validateUser() {
        return new Account("moazzam", "khan");
    }

}
