package me.moazzam.repository;

import me.moazzam.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface ItemRepository extends JpaRepository<Item, Long> {
    Collection<Item> findByParentId(Long id);
    List<Item> findAll();
}