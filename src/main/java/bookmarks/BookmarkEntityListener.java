package bookmarks;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class BookmarkEntityListener {
    @PreUpdate
    public void preUpdate(Bookmark b) {

        System.out.println("preUpdate");
    }

    @PrePersist
    public void prePersist(Bookmark b) {
        System.out.println(b.getId());
    }

    @PostPersist
    public void postPersist(Bookmark b) {
        System.out.println(b.getId());
    }
}
